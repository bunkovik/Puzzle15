#include <iostream>
#include "src/game.h"

using namespace std;

int main(int argc, char* argv[]) {
    Game game;

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];
        // Display help information
        if (argc == 2 && string(argv[1]) == "--help") {
            game.printHelp();
            return EXIT_SUCCESS;
        }
    }

    int boardSize;
    if (argc == 2) {
        try {
            boardSize = std::stoi(argv[1]);
        } catch (const std::invalid_argument& e) {
            if (boardSize < 2) {
                std::cerr << "Invalid argument. Board size must be 2 or greater." << std::endl;
                return 1;
            }
        }
    } else {
        std::cout << "Enter the size of the board (for instance: 3 - means 3x3, 4 - means 4x4) : ";

        while (!(std::cin >> boardSize) || std::cin.peek() != '\n'|| boardSize < 2) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Invalid input. Please enter a valid integer: ";
        }
    }
    std::cout << "Board size: " << boardSize << std::endl;

    game.setRows(boardSize);
    game.setColumns(boardSize);

    game.start();

    return EXIT_SUCCESS;
}