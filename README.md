# Game Puzzle15



## Zadání

Zadáním této semestrální práce je vytvořit hru podle vlastního návrhu. Pro splnění zadání jsem zvolila hru Puzzle 15(or Sliding 15 Puzzle). Hru lze zahrát přes terminál. 
Hra běží ve více vláknech: vstup, výstup a výpočet.
- inputThread() - zpracovává uživatelský vstup, včetně příkazu exit 'q'.
- outputThread() - vytiskne aktuální stav desky, když není vyřešen, a poskytuje dostupné tahy.
- computeThread() - inicializuje desku a kontroluje řešitelnost. Upozorní výstupní vlákno, aby vytisklo úvodní desku.

## Pravidla hry a Ovládání programu

Tato klasická posuvná logická hra vás vyzývá k uspořádání očíslovaných dlaždic ve vzestupném pořadí v mřížce jake zvolite. 

Jak hrát:


1. Zadejte velikost desky (například: 3 - znamená 3x3, 4 - znamená 4x4):

2. K přesunutí dlaždic použijte následující klávesy:
   - 'w' pro posunutí dlaždice nahoru
   - 's' pro posunutí dlaždice dolů
   - 'a' pro posunutí dlaždice doleva
   - 'd' pro posunutí dlaždice doprava
   - 'q' pro ukončení hry

3. Cíl:
- Uspořádejte očíslované dlaždice ve vzestupném pořadí a ponechte dlaždici v pravém dolním rohu  prázdnou.


4. Vítězství:
- Hru vyhrajete, když jsou všechny očíslované dlaždice v pořádku a volná dlaždice je vpravo dole.


## Kompilace programu
Program lze zkompilovat následujícím příkazem:

```cpp
cmake --build cmake-build-debug
```
nebo
```cpp
g++ -o main main.cpp src/game.cpp
```
## Spuštění hry
Program lze spustit pomocí tlačítka Run přimo v CLION nebo 
z příkazové řádky s možností specifikace velikosti hracího pole:

```cpp
./main [velikost_pole]
```
Například pro 4x4:
```cpp
./main 4
```

## Help command
Přepínač --help zobrazí nápovědu a pravidla hry:

```cpp
./main --help
```

## Testování programu
Pro testování byl použit algoritmus, který kontroluje, zda náhodně generované pole má řešení.

### Board Solvability Algorithm
Přehled algoritmu:
Algoritmus pro kontrolu řešitelnosti desky je založen na počítání inverzí a určení, zda lze desku vyřešit. Inverze představují dvojice destiček, kde se destička s vyšší hodnotou objeví před destičkou s nižší hodnotou při počítání zleva doprava, shora dolů na herním plánu.

Algoritmus zahrnuje dvě klíčové funkce:
- getInvCount() -Prochází dvojicemi destiček na herním plánu a počítá počet převrácení.
Inverze se počítá, když se dlaždice s vyšší hodnotou objeví před dlaždicí s nižší hodnotou.
- findXPosition()-Vyhledá pozici řádku prázdného místa na hrací desce (reprezentované „ “).

Pak funkce:
- isSolvable() -Vypočítá počet inverzí pomocí getInvCount().
Určuje, zda je deska řešitelná na základě počtu inverzí a pozice prázdného místa.
Pokud je velikost mřížky lichá, deska je řešitelná, pokud je počet inverzí sudý.
Pokud je velikost mřížky sudá, je hrací deska řešitelná, pokud je součet počtu inverzí a pozice řádku prázdného místa (počítáno odspoda) sudý.

## Memory check
Pro kontrolu úniku paměti byl použit valgrind. Zde je výsledek valgrindu:

- ==2202== 
- ==2202== HEAP SUMMARY:
- ==2202==     in use at exit: 1,248 bytes in 7 blocks
- ==2202==   total heap usage: 15 allocs, 8 frees, 76,992 bytes allocated
- ==2202== 
- ==2202== LEAK SUMMARY:
- ==2202==    definitely lost: 0 bytes in 0 blocks
- ==2202==    indirectly lost: 0 bytes in 0 blocks
- ==2202==      possibly lost: 864 bytes in 3 blocks
- ==2202==    still reachable: 384 bytes in 4 blocks
- ==2202==         suppressed: 0 bytes in 0 blocks
- ==2202== Rerun with --leak-check=full to see details of leaked memory
- ==2202== 
- ==2202== For lists of detected and suppressed errors, rerun with: -s
- ==2202== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)

