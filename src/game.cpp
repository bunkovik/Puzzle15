//
// Created by bunkovik on 08.12.2023.
//
#include <iostream>
#include <sstream>
#include <thread>
#include "game.h"

using namespace std;

Game::Game() = default;
Game::~Game() = default;

void Game::inputThread() {
    while (true) {
        char cNextMove;
        cin >> cNextMove;
        if(cNextMove == 'q'){
            cout<< "Command q = EXIT was used!";
            exit(0);
        }
        unique_lock<mutex> lock(mtx);

        if (quitRequested) {
            lock.unlock();
            break;
        }

        EMove eNextMove = static_cast<EMove>(cNextMove);
        Move(eNextMove);
        moves++;
        cout << "Number of moves: " << moves << endl;

        inputCvar.notify_one();
        lock.unlock();
    }
}

void Game::outputThread() {
    unique_lock<mutex> lock(mtx);
    while (!quitRequested) {
        outputCvar.wait(lock);

        if (!isBoardSolved() && isSolvable()) {
            PrintBoard();
            cout << endl << "w = Up, s = Down, a = Left, d = Right, q = EXIT" << endl;
        }
    }
}

void Game::computeThread() {
    unique_lock<mutex> lock(mtx);
    do {
        InitializeBoard();
    } while (!isSolvable() || isBoardSolved());

    outputCvar.notify_one();
    lock.unlock();

    while (!quitRequested) {
        unique_lock<mutex> lock(mtx);
        inputCvar.wait(lock);
        if (isBoardSolved()) {
            cout << endl << "Congratulations! You have won the game!" << endl;
            PrintBoard();
            cout << "Number of moves: " << moves << endl;
            quitRequested = true;

            exit(0);
        } else {
            outputCvar.notify_one();
        }
        lock.unlock();
    }
    outputCvar.notify_all();
    inputCvar.notify_all();
}

void Game::start(){
    thread input(&Game::inputThread, this);
    thread output(&Game::outputThread, this);
    thread compute(&Game::computeThread, this);

    input.join();
    output.join();
    compute.join();
}

void Game::InitializeBoard() {
    srand(static_cast<unsigned int>(time(0)));
    std::vector<std::string> numbers;


    for (int i = 1; i <= rows * columns - 1; ++i) {
        numbers.push_back(to_string(i));
    }

    numbers.push_back(" ");
    std::random_shuffle(numbers.begin(), numbers.end());
    board = numbers;
}

void Game::PrintBoard() {
    if (board.empty()) {
        std::cout << "" << std::endl;
        return;
    }

    int lengthOfLongestElement = 0;
    for (const std::string& element : board) {
        if (element.length() > lengthOfLongestElement) {
            lengthOfLongestElement = element.length();
        }
    }

    int punctLine = ((lengthOfLongestElement + 2) * columns) + columns + 1;

    std::cout<< std::setw(punctLine) << std::setfill('-') << "" << std::endl;

    int index = 0;
    for (const std::string& element : board) {
        std::cout << "|"  << std::setw(lengthOfLongestElement + 2);

        if (element == " ") {
            std::cout << std::setfill(' ') << " " ;
        } else {
            std::cout  << std::setfill(' ') << element ;
        }

        if ((index + 1) % columns == 0) {
            std::cout  << "|" <<  std::endl;
        }
        index++;
    }

    std::cout << std::setw(punctLine) << std::setfill('-') << ""  << std::endl;
}

void Game::LocateSpace(int& irRow, int& irCol) {
    int size = board.size();
    for (int i = 0; i < size; ++i) {
        if (board[i] == " ") {
            irRow = i / columns;
            irCol = i % columns;
            return;
        }
    }
}

void Game::Move( const EMove keMove) {
    int iRowSpace, iColSpace;
    LocateSpace( iRowSpace, iColSpace);
    int iRowMove(iRowSpace);
    int iColMove(iColSpace);


    switch (keMove) {
        case EMove::keUp:
            iRowMove = iRowSpace + 1;
            break;
        case EMove::keDown:
            iRowMove = iRowSpace - 1;
            break;
        case EMove::keLeft:
            iColMove = iColSpace + 1;
            break;
        case EMove::keRight:
            iColMove = iColSpace - 1;
            break;
    }

    // is square to be moved  in bounds?
    if (iRowMove >= 0 && iRowMove < rows && iColMove >= 0 && iColMove < columns) {
        std::swap(board[iRowSpace * columns + iColSpace], board[iRowMove * columns + iColMove]);
    }
}

bool Game::isBoardSolved() {
    int gridPositionExpectedValue = 1;

    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            int index = i * columns + j;

            if (board[index] != std::to_string(gridPositionExpectedValue) &&
                board[index] != " ") {
                return false;
            }
            gridPositionExpectedValue++;
        }
    }
    return true;
}

int Game::getInvCount() {
    int inv_count = 0;
    int size = board.size();

    for (int i = 0; i < size - 1; i++) {
        for (int j = i + 1; j < size; j++) {
            // count pairs(ccaboard[i], ccaboard[j]) such that i < j but ccaboard[i] > ccaboard[j]
            if (board[j] != " " && board[i] != " " && stoi(board[i]) > stoi(board[j])) {
                inv_count++;
            }
        }
    }
    return inv_count;
}

int Game::findXPosition() {
    int size = board.size();
    for (int i = 0; i < size; i++) {
        if (board[i] == " ") {
            return i / columns;
        }
    }
    return -1;
}

bool Game::isSolvable() {
    int invCount = getInvCount();

    // If grid is odd, return true if inversion count is even.
    if ((rows * columns) & 1)
        return !(invCount & 1);
    else {
        int pos = findXPosition();
        if (pos & 1)
            return !(invCount & 1);
        else
            return invCount & 1;
    }
}

void Game::printHelp(){
    cout << "Welcome to Puzzle 15!" << endl;
    cout << "Puzzle 15 is a sliding puzzle game with a board of numbered tiles and one blank space." << endl;
    cout << "The objective is to arrange the tiles in ascending order, starting from the top-left corner." << endl;
    cout << "You can slide tiles into the blank space to rearrange them." << endl << endl;

    cout << "Usage:" << endl;
    cout << "./main [boardSize]" << endl;
    cout << "Example: ./main 4 (for a 4x4 board)" << endl;
    cout << "Or" <<endl;
    cout << "You can just run the game using run button and then: " << endl;
    cout << "- Enter the size of the board when prompted (e.g., 3 for a 3x3 board)." << endl << endl;

    cout << "Rules:" << endl;
    cout << "- Use 'w' to move up, 's' to move down, 'a' to move left, and 'd' to move right." << endl;
    cout << "- Use q - to exit from the program" << endl;
    cout << "- Try to arrange the tiles in ascending order to win the game." << endl << endl;

    cout << "Have fun playing Puzzle 15!" << endl;
}

void Game::setRows(int rowsSize) {
    this->rows = rowsSize;
}

void Game::setColumns(int columnsSize) {
    this->columns = columnsSize;
}
