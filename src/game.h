#ifndef MYPUZZLE15_GAME_H
#define MYPUZZLE15_GAME_H

#include <iostream>
#include <algorithm>
#include <ctime>
#include <vector>
#include <iomanip>
#include <random>
#include <thread>
#include <mutex>
#include <condition_variable>
enum class EMove {
    keUp = 'w',
    keDown = 's',
    keLeft = 'a',
    keRight = 'd'
};

class Game{
public:
    Game();
    ~Game();
    /**
   * Starts the puzzle game by launching input, output, and compute threads.
   *
   * @return void
   */
    void start();

    /**
     * Prints help information about the puzzle game.
     *
     * @return void
     */
    void printHelp();

    /**
     * Sets the number of rows for the puzzle board.
     *
     * @param rowsSize Number of rows for the board
     * @return void
     */
    void setRows(int rowsSize);

    /**
     * Sets the number of columns for the puzzle board.
     *
     * @param columnsSize Number of columns for the board
     * @return void
     */
    void setColumns(int columnsSize);
    volatile bool quitRequested = false; /**< Indicates whether the program should exit. */

private:
    std::vector<std::string> board; /**< Represents the game board with tiles. */
    int rows, columns; /**< Number of rows and columns in the game board. */
    int moves = 0; /**< Tracks the number of moves made during the game. */
    std::mutex mtx; /**< Mutex for ensuring thread safety. */
    std::condition_variable inputCvar, outputCvar;/**< Condition variable for synchronizing input and output thread. */

/**
     * Thread for processing user input.
     *
     * @return void
     */
    void inputThread();

    /**
     * Thread for displaying the current state of the board.
     *
     * @return void
     */
    void outputThread();

    /**
     * Thread for computing and updating the board state.
     *
     * @return void
     */
    void computeThread();

    /**
     * Initializes the puzzle board with a random arrangement of tiles.
     *
     * @return void
     */
    void InitializeBoard();

    /**
     * Prints the current state of the puzzle board.
     *
     * @return void
     */
    void PrintBoard();

    /**
     * Locates the position of the empty space on the puzzle board.
     *
     * @param irRow Reference to store the row of the empty space
     * @param irCol Reference to store the column of the empty space
     * @return void
     */
    void LocateSpace(int& irRow, int& irCol);

    /**
     * Moves a tile on the puzzle board based on the provided move direction.
     *
     * @param keMove The direction in which to move a tile
     * @return void
     */
    void Move(const EMove keMove);

    /**
     * Checks if the puzzle board is in a solved state.
     *
     * @return True if the board is solved, false otherwise
     */
    bool isBoardSolved();

    /**
     * Calculates the inversion count of the puzzle board.
     *
     * @return The inversion count
     */
    int getInvCount();

    /**
     * Finds the row position of the empty space on the puzzle board.
     *
     * @return The row position of the empty space
     */
    int findXPosition();

    /**
     * Checks if the puzzle board is solvable.
     *
     * @return True if the board is solvable, false otherwise
     */
    bool isSolvable();

};
#endif //MYPUZZLE15_GAME_H